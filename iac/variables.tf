variable "service_name" {
  description = "The service name."
  type        = string
}

variable "cloud_function_zip_name" {
  description = "The name for cloud function zip for a non ."
  type        = string
}

variable "env" {
  description = "The environnement."
  type        = string
  default     = "dev"
}