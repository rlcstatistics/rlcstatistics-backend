data "archive_file" "default" {
  type        = "zip"
  output_path = "/tmp/function-source.zip"
  source_dir  = "../src/"
}

locals {
  bucket_name = "gcf-v2-uploads-109912438483-europe-west9"
  trigger_service_account = "scheduler@rlcstatistics.iam.gserviceaccount.com"
}

resource "google_storage_bucket_object" "object" {
  name   = "${var.cloud_function_zip_name}.zip"
  bucket = local.bucket_name
  source = data.archive_file.default.output_path
}

resource "google_cloudfunctions2_function" "cloud_function" {
  name        = var.service_name
  location    = "europe-west9"
  description = ""
  project     = "rlcstatistics"

  build_config {
    runtime     = "python311"
    entry_point = "main_http"
    environment_variables = {
        ENV = var.env
    }
    source {
      storage_source {
        bucket = local.bucket_name
        object = google_storage_bucket_object.object.name
      }
    }
  }

  service_config {
    max_instance_count = 1
    available_memory   = "1Gi"
    timeout_seconds    = 120
    service_account_email = "bucket@rlcstatistics.iam.gserviceaccount.com"
    ingress_settings = "ALLOW_INTERNAL_ONLY"
    environment_variables = {
        ENV = var.env
    }
  }
}

resource "google_cloudfunctions2_function_iam_member" "member" {
  project = google_cloudfunctions2_function.cloud_function.project
  location = google_cloudfunctions2_function.cloud_function.location
  cloud_function  = google_cloudfunctions2_function.cloud_function.name
  role     = "roles/cloudfunctions.invoker"
  member   = "serviceAccount:${local.trigger_service_account}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_invoker" {
  project = google_cloudfunctions2_function.cloud_function.project
  location = google_cloudfunctions2_function.cloud_function.location
  service  = google_cloudfunctions2_function.cloud_function.name
  role     = "roles/run.invoker"
  member   = "serviceAccount:${local.trigger_service_account}"
}

output "function_uri" {
  value = google_cloudfunctions2_function.cloud_function.service_config[0].uri
}

resource "google_cloud_scheduler_job" "scheduler_to_trigger_cloud_function" {
  name             = var.service_name
  description      = "Job to trigger Cloud Function ${var.service_name}"
  schedule         = "*/30 * * * *"
  time_zone        = "Europe/Paris"
  attempt_deadline = "180s"
  project          = "rlcstatistics"
  region           = "europe-west1"
  paused           = var.env == "prd" ? false : true

  http_target {
    http_method = "POST"
    uri         = google_cloudfunctions2_function.cloud_function.url

    oidc_token {
      service_account_email = "${local.trigger_service_account}"
    }
  }
}