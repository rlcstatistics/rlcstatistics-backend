##########
# Stages #
##########
stages:
  - prepare
  - test:technical
  - version
  - deploy

########################
# Default Docker Image #
########################
image: python:3.9

##############
# Hiden Jobs #
##############
.triggers:
  merge_request: &trigger_merge_request
    if: $CI_MERGE_REQUEST_ID
  commit_develop: &trigger_commit_develop
    if: $CI_COMMIT_BRANCH =~ /^develop$/
  commit_main: &trigger_commit_main
    if: $CI_COMMIT_BRANCH =~ /^main$/
  tag: &trigger_tag
    if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v\d{1,}\.\d{1,}\.\d{1,}$/

.skips:
  version_changes_dev: &skip_version_changes_dev
    if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^develop$/
    changes:
      paths:
        - VERSION
    when: never
  version_changes: &skip_version_changes
    if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^main$/ && $CI_COMMIT_TAG !~ /^v.*$/
    changes:
      paths:
        - VERSION
    when: never

.cache:
  cache: &cache
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .venv

.cache_pull:
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths: !reference [.cache, cache, paths]
    policy: pull

.make_scripts_files_executable:
  before_script:
    - find scripts -type f -exec chmod +x {} \;

.test:technical:
  stage: test:technical
  extends:
    - .make_scripts_files_executable
  rules:
    - <<: *trigger_merge_request
    - <<: *trigger_commit_develop
    - <<: *trigger_commit_main

.test:technical:unitest:
  extends:
    - .test:technical
    - .cache_pull
  needs: [dependencies:download]
  script:
    - ls
    - scripts/unitest.sh

.test:technical:pylint:
  extends:
    - .test:technical
    - .cache_pull
  needs: [dependencies:download]
  script:
    - scripts/pylint.sh

############
# Workflow #
############
workflow:
  rules:
    - <<: *skip_version_changes_dev
    - <<: *skip_version_changes
    - when: always

################
# Generic Jobs #
################
dependencies:download:
  extends:
    - .cache
    - .make_scripts_files_executable
  stage: prepare
  script:
    - scripts/dependencies_download.sh
  rules:
    - <<: *trigger_merge_request
    - <<: *trigger_commit_develop
    - <<: *trigger_commit_main

version:
  stage: version
  extends:
    - .make_scripts_files_executable
  parallel:
    matrix:
      - VERSION_LEVEL: [major, minor, patch]
  script:
    - scripts/version.sh
  rules:
    - <<: *trigger_commit_main
      when: manual

################
# Serivce Jobs #
################
.service:backend:
  variables:
    SERVICE: backend

test:technical:backend:pylint:
  extends:
    - .test:technical:pylint
    - .service:backend

test:technical:backend:unitest:
  extends:
    - .test:technical:unitest
    - .service:backend

deploy:backend:dev:
  image:
    name: hashicorp/terraform:light
    entrypoint: [""]
  stage: deploy
  variables:
    TF_VAR_service_name: rlcstatistics-backend-dev
    TF_VAR_docker_image: $GCP_REGISTRY/rlcstatistics/$CI_PROJECT_NAME/$CI_PROJECT_NAME:$CI_COMMIT_SHORT_SHA
    TF_VAR_cloud_function_zip_name: function-source-$CI_COMMIT_SHORT_SHA
    TF_VAR_env: dev
  script:
    - cp $GCP_KEY /tmp/credentials.json
    - export GOOGLE_APPLICATION_CREDENTIALS="/tmp/credentials.json"
    - cd iac
    - terraform init -backend-config dev.backend.config
    - terraform apply -auto-approve
  extends:
    - .service:backend
  rules:
    - <<: *trigger_commit_develop

deploy:backend:prod:
  image:
    name: hashicorp/terraform:light
    entrypoint: [""]
  stage: deploy
  variables:
    TF_VAR_service_name: rlcstatistics-backend-prod
    TF_VAR_docker_image: $GCP_REGISTRY/rlcstatistics/$CI_PROJECT_NAME/$CI_PROJECT_NAME:$CI_COMMIT_TAG
    TF_VAR_cloud_function_zip_name: function-source-$CI_COMMIT_SHORT_SHA
    TF_VAR_env: prd
  before_script:
    - cp $GCP_KEY /tmp/credentials.json
    - export GOOGLE_APPLICATION_CREDENTIALS="/tmp/credentials.json"
  script:
    - cp $GCP_KEY /tmp/credentials.json
    - export GOOGLE_APPLICATION_CREDENTIALS="/tmp/credentials.json"
    - cd iac
    - terraform init -backend-config prod.backend.config
    - terraform apply -auto-approve
  extends:
    - .service:backend
  rules:
    - <<: *trigger_tag

