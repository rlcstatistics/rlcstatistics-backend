import os, json
from google.cloud import storage


class IndexGCP():
    def __init__(self):
        self.BUCKET_NAME = "rlcstatistics"
        self.client = storage.Client()
        self.bucket = self.client.bucket(self.BUCKET_NAME)
        self.ENV = os.getenv("ENV", "dev")
    
    def list_gcs_files(self) -> list[str]:
        blobs = self.bucket.list_blobs(prefix=f"{self.ENV}/matches/status=done/")
        return [blob.name for blob in blobs]

    def extract_match_data(self, blob_content: str) -> dict:
        match_data = json.loads(blob_content)
        match_info = {
            "match_id": match_data["_id"],
            "date": match_data["date"],
            "event_name": match_data["event"]["name"],
            "blue_team_name": match_data["blue"]["team"]["team"]["name"],
            "orange_team_name": match_data["orange"]["team"]["team"]["name"]
        }
        return match_info
    
    def create_index(self) -> list[dict]:
        index = []
        file_names = self.list_gcs_files()
        for file_name in file_names:
            blob = self.bucket.blob(file_name)
            content = blob.download_as_string()
            match_data = self.extract_match_data(content)
            index.append(match_data)
        return index
    
    def save_index_to_gcs(self, index: list[dict]):
        blob = self.bucket.blob(f"{self.ENV}/matches/index.json")
        blob.upload_from_string(data=json.dumps(index), content_type="application/json")


if __name__ == "__main__":
    idx = IndexGCP()
    index = idx.create_index()
    idx.save_index_to_gcs(index)