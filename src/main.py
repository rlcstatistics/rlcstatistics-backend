import functions_framework
from io import BytesIO
import os, requests, json, time, math
from datetime import datetime
from google.cloud import storage
import pandas as pd 


class DAOMatchesGCP():
    def __init__(self):
        self.BUCKET_NAME = "rlcstatistics"
        self.client = storage.Client()
        self.bucket = self.client.bucket(self.BUCKET_NAME)
        self.ENV = os.getenv("ENV", "dev")
    
    def is_upcoming_match_exist(self, id_match: str) -> bool:
        files_list = list(self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/matches/status=upcoming/id_match={id_match}'))
        return len(files_list) > 0
    
    def fetch_team_name_from_match(self, match: json, color: str) -> str:
        return match[color]["team"]["team"]["name"]
        
    def get_upcoming_matches(self) -> None:
        today = datetime.today().strftime('%Y-%m-%dT%H:%M:%SZ')
        url=f"https://zsr.octane.gg/matches?after={today}"
        json_results = requests.get(url).json()
        for match in json_results["matches"]:
            if 'orange' not in match or 'blue' not in match:
                continue
            if self.is_upcoming_match_exist(id_match=match["_id"]):
                continue
            self.add_upcoming_match(match)
            self.fetch_teams_image(match=match)

    def add_upcoming_match(self, match: json):
        orange_team_name, blue_team_name = self.fetch_team_name_from_match(match=match, color="orange"), self.fetch_team_name_from_match(match=match, color="blue")
        key = f'{self.ENV}/matches/status=upcoming/id_match={match["_id"]}&team_name_orange={orange_team_name}&team_name_blue={blue_team_name}&start_date={match["date"]}'
        self.bucket.blob(key).upload_from_string("")
        
    def give_reward_bets_match(self, match: json) -> None:
        id_match, orange_team_name, blue_team_name = match["_id"], self.fetch_team_name_from_match(match=match, color="orange"), self.fetch_team_name_from_match(match=match, color="blue")
        winner_color, team_name_winner = self.select_winner_and_color_match(match=match)
        email_winners = [blob.name.split("/")[-1].split("||coins=")[0] for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/matches/id_match={match["_id"]}/team_name={team_name_winner}') if blob.name[-1] != '/']
        odds = self.convert_sum_bets_to_odds(self.get_sum_bets_match(id_match=id_match, team_name=blue_team_name), self.get_sum_bets_match(id_match=id_match, team_name=orange_team_name))
        for winner in email_winners:
            self.give_reward_user_bet(email=winner, id_match=id_match, winner_odd=odds[winner_color])

    def select_winner_and_color_match(self, match: json): 
        return ("orange", self.fetch_team_name_from_match(match=match, color="orange")) if match["orange"].get("score", 0) > match["blue"].get("score", 0) else ("blue", self.fetch_team_name_from_match(match=match, color="blue"))

    def get_sum_bets_match(self, id_match: str, team_name: str) -> tuple[str, int]:
        return (team_name, sum([int(blob.name.split('||coins=')[-1]) for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/matches/id_match={id_match}/team_name={team_name}') if blob.name[-1] != '/']))
    
    def convert_sum_bets_to_odds(self, blue_sum: tuple[str, int], orange_sum: tuple[str, int]) -> dict[str, float]:
        sum = blue_sum[1] + orange_sum[1] + 200
        return {"blue": 1/((blue_sum[1]+100)/sum), "orange": 1/((orange_sum[1]+100)/sum)}

    def give_reward_user_bet(self, email: str, id_match: str, winner_odd: int) -> None:
        user_blob, bet_blob = self.bucket.blob(f'{self.ENV}/users/{email}'), self.bucket.blob(f'{self.ENV}/bets/users/{email}/{id_match}')
        user_data, bet_data = json.loads(user_blob.download_as_text()), json.loads(bet_blob.download_as_text())
        reward = math.floor(int(bet_data["amount"]) * winner_odd)
        bet_data["reward"] = reward
        user_data["coins"] += reward
        user_blob.upload_from_string(json.dumps(user_data))
        bet_blob.upload_from_string(json.dumps(bet_data))

    def transform_upcoming_to_match(self) -> None:
        blobs = list(self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/matches/status=upcoming'))
        for blob in blobs: 
            id_match = blob.name.split("/")[-1].split("&")[0].split("=")[1]
            match = requests.get(f"https://zsr.octane.gg/matches/{id_match}").json()
            if "blue" not in match or 'orange' not in match['orange']:
                print(f"Issue with {id_match}")
                continue
            if "winner" not in match['blue'] and 'winner' not in match['orange']:
                print(f"Issue with {id_match}")
                continue
            self.give_reward_bets_match(match=match)
            self.delete_bets_from_upcoming_match(match=match)
            self.update_teams_stats(match=match)
            done_key = f'{self.ENV}/matches/status=done/id_match={match["_id"]}'
            self.bucket.blob(done_key).upload_from_string(json.dumps(match))
            blob.delete()

    def delete_bets_from_upcoming_match(self, match: json) -> None:
        try:
            [blob.delete() for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/matches/id_match={match["_id"]}')]
        except Exception as e:
            print(f"Error occurs when deleting bets from upcoming match {match['_id']}")
            pass

    def update_teams_stats(self, match: json) -> None:
        self.update_team_stats(match=match, color="blue")
        self.update_team_stats(match=match, color="orange")
        
    def update_team_stats(self, match: json, color: str) -> None:
        team_name = self.fetch_team_name_from_match(match=match, color=color)
        blob = self.bucket.blob(f"{self.ENV}/teams/{team_name}.csv")
        try:
            df = pd.read_csv(BytesIO(blob.download_as_bytes()))
            if len(df) >= 5: df.drop(0, axis=0, inplace=True).reset_index(drop=True)
        except Exception as e:
            columns = ['score', 'goals', 'shots', 'assists', 'saves']
            df = pd.DataFrame(columns=columns)
        try:
            main_stats = match[color]["team"]["stats"]["core"]
            stats = {
                'score': main_stats["score"], 
                'goals': main_stats["goals"], 
                'shots': main_stats["shots"], 
                'assists': main_stats["assists"], 
                'saves': main_stats["saves"]
            }
            new_row = pd.DataFrame([stats])
            new_df = pd.concat([df, new_row], ignore_index=True)
            blob.upload_from_file(BytesIO(new_df.to_csv(index=False).encode()), content_type='text/csv')
        except Exception as e:
            pass

    def check_file_exists(self, path: str) -> bool:
        return self.bucket.blob(path).exists()
    
    def fetch_teams_image(self, match: json) -> None:
        try:
            self.fetch_team_image(match=match, color="orange")
            self.fetch_team_image(match=match, color="blue")
        except Exception as e:
            pass
    
    def fetch_team_image(self, match: json, color: str) -> None:
        team_name = self.fetch_team_name_from_match(match=match, color=color)
        path_image = f'{self.ENV}/teams/{team_name}.png'
        if self.check_file_exists(path_image): return None
        response = requests.get(match[color]["team"]["team"]["image"]).content
        blob = self.bucket.blob(path_image)
        blob.upload_from_file(BytesIO(response), content_type='image/png')

    def update_leaderboard(self) -> None:
        columns = ['email', 'username', 'coins']
        leaderboard = pd.DataFrame(columns=columns)
        leaderboard_key = f'{self.ENV}/leaderboard.csv'
        leaderboard_blob = self.bucket.blob(leaderboard_key)
        users = [blob.name.split("/")[-1] for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/users')]
        for email in users: 
            user_data = json.loads(self.bucket.blob(f'{self.ENV}/users/{email}').download_as_text())
            stats = {
                "email": email,
                "username": user_data["username"],
                "coins": user_data["coins"]
            }
            new_row = pd.DataFrame([stats])
            leaderboard = pd.concat([leaderboard, new_row], ignore_index=True)
        leaderboard.sort_values(by="coins", ascending=False, inplace=True)
        leaderboard_blob.upload_from_file(BytesIO(leaderboard.to_csv(index=False).encode()), content_type='text/csv')


@functions_framework.http
def main_http(request):
# if __name__ == "__main__":
    start_time = time.time()
    dao = DAOMatchesGCP()
    dao.get_upcoming_matches()
    dao.transform_upcoming_to_match()
    dao.update_leaderboard()
    end_time = time.time()
    # print(f"Process done within {end_time - start_time} seconds.")
    return f"Process done within {end_time - start_time} seconds."
